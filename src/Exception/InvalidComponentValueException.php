<?php

namespace _2cW\PasswordGenerator\Exception;

/**
 * InvalidComponentNameException.php
 *
 * PHP Password Generator Exception.
 *
 * @package _2cW\PasswordGenerator\Exception
 * @author Mateusz Bajda <mateusz.bajda@2cw.pl>
 * @copyright 2020 2cW.pl
 * @license BSD https://www.freebsd.org/copyright/freebsd-license.html
 */
class InvalidComponentValueException extends \Exception
{

}