<?php

namespace _2cW\PasswordGenerator;

use _2cW\PasswordGenerator\Exception\InvalidComponentNameException;
use _2cW\PasswordGenerator\Exception\InvalidComponentValueException;
use _2cW\PasswordGenerator\Exception\InvalidPasswordLengthException;
use _2cW\PasswordGenerator\Exception\UnexpectedEncryptionErrorException;

/**
 * PasswordGenerator.php
 *
 * PHP Password Generator.
 * Class used for generating passwords satisfying certain conditions
 * like minimum number of character classes' characters.
 *
 * @package _2cW\PasswordGenerator
 * @author Mateusz Bajda <mateusz.bajda@2cw.pl>
 * @copyright 2020 2cW.pl
 * @license BSD https://www.freebsd.org/copyright/freebsd-license.html
 */
class PasswordGenerator
{

    private $lowerCaseLetters = 3;
    private $upperCaseLetters = 3;
    private $digits = 2;
    private $specialChars = 1;

    /**
     * Sets password component requirements
     *
     * @param string       $name  Component name
     * @param bool|integer $value Requirement value
     * @throws InvalidComponentNameException
     * @throws InvalidComponentValueException
     */
    private function setComponent($name, $value)
    {
        $componentNames = [
            'lowerCaseLetters',
            'upperCaseLetters',
            'digits',
            'specialChars',
        ];

        if (!in_array($name, $componentNames)) {
            throw new InvalidComponentNameException('Invalid component name.');
        }

        if ($value !== false && (!is_int($value) || $value < 0)) {
            throw new InvalidComponentValueException('Invalid component value. Valid values include integers greater than 0 or \'false\' value.');
        }

        $this->$name = $value;
    }

    /**
     * Lower case letters.
     * Sets minimum count for lower case letters, or disables them if $count equals to 'false'
     *
     * @param bool|integer $count Minimum character count
     * @return PasswordGenerator
     * @throws InvalidComponentNameException
     * @throws InvalidComponentValueException
     */
    public function lowerCaseLetters($count = false): self
    {
        $this->setComponent('lowerCaseLetters', $count);
        return $this;
    }

    /**
     * Upper case letters.
     * Sets minimum count for upper case letters, or disables them if $count equals to 'false'
     *
     * @param bool|integer $count Minimum character count
     * @return PasswordGenerator
     * @throws InvalidComponentNameException
     * @throws InvalidComponentValueException
     */
    public function upperCaseLetters($count = false): self
    {
        $this->setComponent('upperCaseLetters', $count);
        return $this;
    }

    /**
     * Digits.
     * Sets minimum count for digits, or disables them if $count equals to 'false'
     *
     * @param bool|integer $count Minimum character count
     * @return PasswordGenerator
     * @throws InvalidComponentNameException
     * @throws InvalidComponentValueException
     */
    public function digits($count = false): self
    {
        $this->setComponent('digits', $count);
        return $this;
    }

    /**
     * Special characters.
     * Sets minimum count for special characters, or disables them if $count equals to 'false'
     *
     * @param bool|integer $count Minimum character count
     * @return PasswordGenerator
     * @throws InvalidComponentNameException
     * @throws InvalidComponentValueException
     */
    public function specialChars($count = false): self
    {
        $this->setComponent('specialChars', $count);
        return $this;
    }

    /**
     * Generates password based on previously set (or default) requirements having the length of $length parameter.
     * If password cannot be generated because of minimum characters and password length mismatch, throws exception.
     *
     * @param int $length Length of the generated password
     * @return string
     * @throws InvalidPasswordLengthException
     */
    public function generatePassword(int $length = 15)
    {
        $lowerCaseLetters = 'abcdefghijklmnopqrstuvwxyz';
        $upperCaseLetters = strtoupper($lowerCaseLetters);
        $digits = '0123456789';
        $specialChars = '!@#$%^&*()_+-={}[]:"|;\'\<>?,./';

        $pass = '';
        $passComponents = [];

        $componentNames = [
            'lowerCaseLetters',
            'upperCaseLetters',
            'digits',
            'specialChars',
        ];

        foreach ($componentNames as $componentName) {
            if ($this->$componentName !== false) {
                for ($i = 1; $i <= $this->$componentName; $i ++) {
                    try {
                        $pass .= ${$componentName}[random_int(0, strlen(${$componentName})) - 1];
                    } catch (\Exception $e){
                        throw new UnexpectedEncryptionErrorException($e->getMessage());
                    }
                }
                $passComponents[] = ${$componentName};
            }
        }

        if (strlen($pass) > $length || !count($passComponents)) {
            throw new InvalidPasswordLengthException('Component count mismatch against password length.');
        }

        for ($i = strlen($pass); $i < $length; $i ++) {
            try {
                $componentUsed = $passComponents[random_int(0, count($passComponents) - 1)];
                $pass .= $componentUsed[random_int(0, strlen($componentUsed) - 1)];
            } catch (\Exception $e){
                throw new UnexpectedEncryptionErrorException($e->getMessage());
            }
        }

        return str_shuffle($pass);
    }
}
