# Password Generator Bundle
### Usage
```php
$passGen = new \_2cW\PasswordGenerator\PasswordGenerator();
$passGen->digits(2);
```
The `digits` method ensures that the password will have specified number of digits.
```php
$passGen = new \_2cW\PasswordGenerator\PasswordGenerator();
$passGen->lowerCaseLetters(2);
```
The `lowerCaseLetters` method ensures that the password will have specified number of lower case characters.
```php
$passGen = new \_2cW\PasswordGenerator\PasswordGenerator();
$passGen->upperCaseLetters(2);
```
The `upperCaseLetters` method ensures that the password will have specified number of upper case characters.
```php
$passGen = new \_2cW\PasswordGenerator\PasswordGenerator();
$passGen->specialChars(2);
```
The `specialChars` method ensures that the password will have specified number of special characters.

```php
$passGen = new \_2cW\PasswordGenerator\PasswordGenerator();
$passGen->digits(2)
    ->lowerCaseLetters(1)
    ->upperCaseLetters(1)
    ->specialChars(2);
$passGen->generatePassword(15);
```
The `generatePassword` method generates password according to rules set, having specified number of characters (in this case - `15`).

(c) 2020 2cW.pl