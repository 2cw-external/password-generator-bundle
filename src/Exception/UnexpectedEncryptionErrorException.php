<?php

namespace _2cW\PasswordGenerator\Exception;

/**
 * InvalidComponentNameException.php
 *
 * PHP Password Generator Exception.
 *
 * @package _2cW\PasswordGenerator\Exception
 * @author Jakub Wójtowicz <jakub.wojtiowicz@2cw.pl>
 * @copyright 2020 2cW.pl
 * @license BSD https://www.freebsd.org/copyright/freebsd-license.html
 */
class UnexpectedEncryptionErrorException extends \Exception
{

}
